package com.zuitt.example;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Contact> contacts = new ArrayList<>();

        Contact contact1 = new Contact();

        contact1.setName("Glyn");
        contact1.setContactNumber("09123456789");
        contact1.setAddress("QC");


        Contact contact2 = new Contact();

        contact2.setName("Maulani");
        contact2.setContactNumber("09123456789");
        contact2.setAddress("San Juan");


        contacts.add(contact1);
        contacts.add(contact2);


        Phonebook contactlist = new Phonebook();

        contactlist.setContactList(contacts);

        contactlist.displayContact();

    }


}
