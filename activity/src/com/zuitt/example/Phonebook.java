package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook extends Contact {
    ArrayList<Contact> contactList = new ArrayList<>();


    public Phonebook(){
        super();
    }
    public Phonebook(String name,String contactNumber,String address, ArrayList<Contact> contactList) {
        super(name,contactNumber,address);
        this.contactList = contactList;
    }

    public ArrayList<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<Contact> contactList) {
        this.contactList = contactList;
    }

    public void displayContact(){
        if(!getContactList().isEmpty()){
            for (Contact contact: getContactList()
                 ) {
                System.out.println("Contact: " + getContactList().indexOf(contact));
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact No.: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println("-------------------------------------------------------------------- ");



            }
        }
        else{
            System.out.println("Phonebook is Empty!");
        }
    }



}
